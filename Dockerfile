FROM node:12-alpine

WORKDIR /app

COPY package.json .
COPY requirements.txt .

RUN npm i
