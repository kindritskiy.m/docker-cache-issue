#!/bin/sh

BUILDER_NAME="docker-image-builder"

install_buildx() {
  if [ ! -f ./docker-buildx ]; then
    echo "Installing buildx docker plugin v0.3.1"
    wget -q https://github.com/docker/buildx/releases/download/v0.3.1/buildx-v0.3.1.linux-amd64
    mv buildx-v0.3.1.linux-amd64 docker-buildx
    chmod +x docker-buildx
  fi
}

create_builder() {
  BUILDER=`./docker-buildx ls  | grep ${BUILDER_NAME} | grep docker-container | awk '{print$1}'`;

  if [ -z ${BUILDER} ]; then
    echo "Creating buildx builder -> ${BUILDER_NAME}"
    ./docker-buildx create --name ${BUILDER_NAME} --use
  fi
}

remove_builder() {
  echo "Removing buildx builder -> ${BUILDER_NAME}"
  ./docker-buildx rm ${BUILDER_NAME}
}

build_image_cache() {
    echo "Building cache for registry.gitlab.com/kindritskiy.m/docker-cache-issue:latest"

    ./docker-buildx build --cache-to type=registry,ref=registry.gitlab.com/kindritskiy.m/docker-cache-issue:latest,mode=max .

    echo "Build cache for registry.gitlab.com/kindritskiy.m/docker-cache-issue:latest"
}

install_buildx;
create_builder;
build_image_cache;
remove_builder;
