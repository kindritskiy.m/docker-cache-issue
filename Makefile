prune:
	echo y | docker builder prune
	echo y | docker system prune -a

build:
	./docker-buildx build -f Dockerfile --cache-from registry.gitlab.com/kindritskiy.m/docker-cache-issue:latest --load .